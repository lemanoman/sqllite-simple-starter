/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.scavengernewgen;

import com.mycompany.scavengernewgen.model.Contact;
import java.util.List;
import java.util.Properties;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
 
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

/**
 *
 * @author Kevim Such
 */
public class Start {
      
    public static void main(String[] args) {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("scavenger-pu");
        EntityManager em = factory.createEntityManager();
        
        Query q = em.createQuery("SELECT u FROM Contact u");
          List<Contact> userList = q.getResultList();
          for (Contact user : userList) {
               System.out.println(user.getName());
          }
          System.out.println("Size: " + userList.size());
    }
}
